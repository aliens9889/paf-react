var React = require('react');
var $ = require('jquery');

var PafInvoiceRequest = React.createClass({

    render: function () {
        return (
            <div className="row invoice">
                <div className="large-12 text-right consultHistory">
                    <a href="">Consultar <strong>Histórico</strong></a>
                </div>
                <div className="invoice large-6 large-offset-3 medium-6 medium-offset-3 small-6 small-offset-3">

                    <form action="">
                        <div className="row">
                            <h1>Solicitud de <strong>Factura</strong></h1>
                            <div className="invoiceAd">
                                <p>La Facturacion Electronica la podrá realizar en el mismo mes que realizo su consumo
                                    (fecha impresa en el Ticket).</p>
                            </div>

                            <label className="control-label required">Sucursal *</label>
                            <input type="text" placeholder="Escribe para seleccionar una sucursal..."/>

                            <label className="control-label required">Monto Ticket *</label>
                            <input data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false"
                                   tabindex="2" title="Ej:156.00" type="number" min="0"/>

                            <label className="control-label required">Ticket Id *</label>
                            <input data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false"
                                   tabindex="2" title="Ej:568921" type="text"/>

                            <div className="row">
                                <div className="large-8 columns rfc">
                                    <label>Mi R.F.C.</label>
                                    <input type="text"/>
                                </div>
                                <div className="large-4 columns checkbox">
                                    <input type="checkbox"/>
                                    <label for="middle-label">Soy Extranjero</label>
                                </div>
                            </div>
                            <p className="small">Si tiene alguna duda o problema para generar su factura, favor de
                                enviar un correo a <a href="#">facturacion@carlogio.com</a>, incluyendo ticket
                                digitalizado y los datos
                                fiscales de facturación completos</p>
                            <div className="text-center">
                                {/*<button className="button">Siguente</button>*/}
                                <input type="submit" className="button nextButton" value="Siguiente"/>
                            </div>

                            <div className="form-footer panel-footer">
                                <p>Los campos con * son requeridos para poder acceder a generar la factura.</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        )
    }

});

module.exports = PafInvoiceRequest;