var React = require('react');

var PafInvoiceRequest = require('PafInvoiceRequest');
var PafHeader = require('PafHeader');
var PafFooter = require('PafFooter');

var PafApp = React.createClass({
    render: function () {
        return (
            <div className="container">
                <PafHeader/>
                <PafInvoiceRequest/>
                <PafFooter/>
            </div>
        )
    }
});

module.exports = PafApp;