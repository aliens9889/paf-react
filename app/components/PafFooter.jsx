var React = require('react');

var PafFooter = React.createClass({

    render: function () {
        return (
            <div className="row rowFooter">
                <div className="footer">
                    <p>
                        Facturación Electrónica | Todos los Derechos Reservados Ambit Inc. 2015 | Licencias | Políticas de Privacidad
                    </p>
                </div>
            </div>

        )
    }

});

module.exports = PafFooter;