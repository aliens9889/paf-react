var webpack = require('webpack');
var path = require('path');

module.exports = {
  entry: [
    'script!jquery/dist/jquery.min.js',
    // 'script!foundation-sites/dist/foundation.min.js',
      'script!foundation-sites/dist/js/foundation.min.js',
      'script!foundation-sites/dist/js/plugins/foundation.tooltip.min.js',
      'script!foundation-sites/js/foundation.core.js',
      'script!foundation-sites/js/foundation.util.box.js',
      'script!foundation-sites/js/foundation.util.mediaQuery.js',
      'script!foundation-sites/js/foundation.util.triggers.js',
      'script!foundation-sites/dist/js/plugins/foundation.core.js',
      'script!foundation-sites/dist/js/plugins/foundation.core.min.js',
      'script!foundation-sites/dist/js/plugins/foundation.util.box.js',
      'script!foundation-sites/dist/js/plugins/foundation.util.box.min.js',
      'script!foundation-sites/dist/js/plugins/foundation.util.mediaQuery.js',
      'script!foundation-sites/dist/js/plugins/foundation.util.mediaQuery.min.js',
      'script!foundation-sites/dist/js/plugins/foundation.util.triggers.js',
      'script!foundation-sites/dist/js/plugins/foundation.util.triggers.min.js',
      'script!foundation-sites/dist/js/plugins/foundation.tooltip.js',
      'script!foundation-sites/js/foundation.tooltip.js',
    './app/app.jsx'
  ],
  externals: {
    jquery: 'jQuery'
  },
  plugins: [
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery'
    })
  ],
  output: {
    path: __dirname,
    filename: './public/bundle.js'
  },
  resolve: {
    root: __dirname,
    modulesDirectories: [
        'node_modules',
        './app/components',
        './app/api'
    ],
    alias: {
      applicationStyles: 'app/styles/app.scss'
    },
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-0']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  },
  sassLoader: {
    includePaths: [
      path.resolve(__dirname, './node_modules/foundation-sites/scss')
    ]
  },
  devtool: 'cheap-module-eval-source-map'
};
